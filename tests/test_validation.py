
import pytest
from jsonschema import ValidationError

from showy.validation import validate_layout


def test_validation():
    layout = {'title': 'SPEC',
              'graphs': [{'curves': [{'var': 'Y_*_max', 'legend': 'max'},
                                     {'var': 'Y_*_mean', 'legend': 'mean'},
                                     {'var': 'Y_*_min', 'legend': 'min'}],
                          'x_var': 'atime',
                          'y_label': 'Mass fraction * [mol/m³/s]',
                          'x_label': 'Time [s]',
                          'title': '* Mass Fraction'}],
              'figure_dpi': 90,
              'figure_size': [14, 8]}

    validate_layout(layout)

    with pytest.raises(ValidationError):
        layout['titlea'] = layout['title']
        validate_layout(layout)
